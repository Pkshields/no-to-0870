﻿using System;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml;
using System.Xml.Linq;
using Windows.Data.Xml.Dom;
using Windows.Networking.Connectivity;

namespace NoTo0870
{
    /// <summary>
    /// Possible errors provided by this class
    /// </summary>
    public enum DBError
    {
        Success,
        NotAValidNumber,
        IncorrectLength,
        NoInternet,
        NotFound,
        ExceptionError
    }

    class Database0870
    {
        /// <summary>
        /// Name of the current company for the number in origNumber
        /// </summary>
        public string Company
        {
            get;
            private set;
        }

        /// <summary>
        /// Original 08* number
        /// </summary>
        public string OrigNumber
        {
            get;
            private set;
        }

        /// <summary>
        /// Replacement number for the 08* number
        /// </summary>
        public string ReplacementNumber
        {
            get;
            private set;
        }

        /// <summary>
        /// DB currently contains some useful data
        /// </summary>
        public bool ContainsData
        {
            get;
            private set;
        }

        /// <summary>
        /// URL to access for the database
        /// </summary>
        const string shellDBString = " http://0870.me/get/xml/";

        /// <summary>
        /// Initialize an instance of Database 0870
        /// </summary>
        public Database0870()
        {
            Company = "";
            OrigNumber = "";
            ReplacementNumber = "";
            ContainsData = false;
        }

        /// <summary>
        /// Check if the number provided is a phone number
        /// </summary>
        /// <param name="numberToCheck">Number to check</param>
        /// <param name="error">Resulting error if there is an error</param>
        /// <returns>Result of check</returns>
        public bool CheckNumber(string numberToCheck, out DBError error)
        {
            //Remove all whitespaces from the number
            string checkNum = numberToCheck.Replace(" ", string.Empty);

            //Check if the passed in string is just a valid string
            double finalNum;
            bool numCheck = double.TryParse(checkNum, out finalNum);
            if (!numCheck)
            {
                error = DBError.NotAValidNumber;
                ContainsData = false;
                return false;
            }

            //Check if the length of the string is correct
            if (checkNum.Length != 10 && checkNum.Length != 11)
            {
                error = DBError.IncorrectLength;
                ContainsData = false;
                return false;
            }

            //Return success!
            error = DBError.Success;
            return true;
        }

        /// <summary>
        /// Get the replacement number data from the 0870 API
        /// </summary>
        /// <param name="numberToCheck">Number to check</param>
        /// <returns>Wether the action was successful or what error occured</returns>
        public async Task<DBError> GetData(string numberToCheck)
        {
            //Check and see if we have internet access
            if (!IsInternet())
            {
                ContainsData = false;
                return DBError.NoInternet;
            }

            //Remove all whitespaces from the number
            numberToCheck.Replace("  ", string.Empty);

            //Create the URI
            Uri urlString = new Uri(shellDBString + numberToCheck);

            //Get the XML data from the 0870.me database
            XmlDocument xmlDocument = await XmlDocument.LoadFromUriAsync(urlString);
            IXmlNode numberNode = xmlDocument.SelectSingleNode("number");

            //Get the company value from the API
            IXmlNode companyNode = numberNode.SelectSingleNode("company");
            string possibleCompany = companyNode.InnerText;

            //If the number is not found, return error
            if (possibleCompany == "Not Found")
            {
                ContainsData = false;
                return DBError.NotFound;
            }

            //Store the company name, as well as the replacement number and origianl number
            Company = possibleCompany;
            IXmlNode altNumNode = numberNode.SelectSingleNode("replacement_number");
            ReplacementNumber = altNumNode.InnerText;
            OrigNumber = numberToCheck;

            //Return success!
            ContainsData = true;
            return DBError.Success;
        }

        /// <summary>
        /// Check if we have access to the internet
        /// </summary>
        /// <returns>If we have internet connection</returns>
        public static bool IsInternet()
        {
            ConnectionProfile connections = NetworkInformation.GetInternetConnectionProfile();
            bool internet = connections != null && connections.GetNetworkConnectivityLevel() == NetworkConnectivityLevel.InternetAccess;
            return internet;
        }
    }
}