﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Threading.Tasks;
using Windows.Foundation;
using Windows.Foundation.Collections;
using Windows.UI.ApplicationSettings;
using Windows.UI.Notifications;
using Windows.UI.Xaml;
using Windows.UI.Xaml.Controls;
using Windows.UI.Xaml.Controls.Primitives;
using Windows.UI.Xaml.Data;
using Windows.UI.Xaml.Input;
using Windows.UI.Xaml.Media;
using Windows.UI.Xaml.Media.Animation;
using Windows.UI.Xaml.Navigation;

// The Basic Page item template is documented at http://go.microsoft.com/fwlink/?LinkId=234237

namespace NoTo0870
{
    /// <summary>
    /// A basic page that provides characteristics common to most applications.
    /// </summary>
    public sealed partial class MainPage : NoTo0870.Common.LayoutAwarePage
    {
        /// <summary>
        /// Access to the 0870.me database
        /// </summary>
        private Database0870 db = null;

        /// <summary>
        /// URL to the privacy policy
        /// </summary>
        private const string privacyPolicyUrl = "http://muzene.com/win8apps/privacypolicy/#noto0870";

        public MainPage()
        {
            this.InitializeComponent();
            db = new Database0870();
        }

        // <summary>
        /// Populates the page with content passed during navigation.  Any saved state is also
        /// provided when recreating a page from a prior session.
        /// </summary>
        /// <param name="navigationParameter">The parameter value passed to
        /// <see cref="Frame.Navigate(Type, Object)"/> when this page was initially requested.
        /// </param>
        /// <param name="pageState">A dictionary of state preserved by this page during an earlier
        /// session.  This will be null the first time a page is visited.</param>
        protected override void LoadState(Object navigationParameter, Dictionary<String, Object> pageState)
        {
            //If we are not at first start
            if (pageState != null)
            {
                //Check if there is a value for DB in the pageState
                object obj;
                if (pageState.TryGetValue("DB", out obj))
                {
                    //There is, get the current database out of the previous state. If  that database has data in it, show it on screen
                    db = (Database0870)obj;
                    if (db.ContainsData)
                        ShowCurrentData();
                }
                else
                    //Else, create a new database
                    db = new Database0870();
                
                //If there is a value for DB in the pageState, pull it out too
                if (pageState.TryGetValue("CurrentNum", out obj))
                    NumBox.Text = (string)obj;
            }
            else
                //First start, create a new database
                db = new Database0870();

            //Add the privacy policy to the settings pane
            SettingsPane.GetForCurrentView().CommandsRequested += MainPage_CommandsRequested;
        }

        /// <summary>
        /// Preserves state associated with this page in case the application is suspended or the
        /// page is discarded from the navigation cache.  Values must conform to the serialization
        /// requirements of <see cref="SuspensionManager.SessionState"/>.
        /// </summary>
        /// <param name="pageState">An empty dictionary to be populated with serializable state.</param>
        protected override void SaveState(Dictionary<String, Object> pageState)
        {
            pageState.Add("DB", db);
            pageState.Add("CurrentNum", NumBox.Text);
        }

        /// <summary>
        /// Code to run when the NumBox loads
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="args"></param>
        private void OnNumBoxLoaded(object sender, RoutedEventArgs args)
        {
            TextBox numBox = sender as TextBox;
            numBox.GotFocus += OnNumBoxGotFocus;
        }

        /// <summary>
        /// Code to run when the NumBox is clicked on
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="args"></param>
        private void OnNumBoxGotFocus(object sender, RoutedEventArgs args)
        {
            TextBox numBox = sender as TextBox;
            if (numBox.Text == "0870")
                numBox.Text = String.Empty;
        }

        /// <summary>
        /// Check the number in the database
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private async void NumCheckButton_Click(object sender, RoutedEventArgs e)
        {
            //Show the loading bar
            LoadingBar.Visibility = Visibility.Visible;

            //Check if the number is legit or not
            DBError error;
            bool result = db.CheckNumber(NumBox.Text, out error);

            //Parse result (if there was an error
            if (!result)
            {
                switch (error)
                {
                    case DBError.NotAValidNumber:
                        ErrorData("Not a valid number.");
                        break;
                    case DBError.IncorrectLength:
                        ErrorData("Not a valid number length.");
                        break;
                }

                //Hide the loading bar and finish method
                LoadingBar.Visibility = Visibility.Collapsed;
                return;
            }

            //Get the data from the API, async style
            error = await db.GetData(NumBox.Text);

            //Check if there was an error, or run succcess code if true
            switch (error)
            {
                case DBError.NoInternet:
                    ErrorData("No internet connection.");
                    break;
                case DBError.NotFound:
                    ErrorData("Number not found.");
                    break;
                case DBError.Success:
                    ShowCurrentData();
                    break;
            }

            //Hide the loading bar
            LoadingBar.Visibility = Visibility.Collapsed;
        }

        /// <summary>
        /// Change the data section of the screen to display an error
        /// </summary>
        /// <param name="error">Error to display</param>
        private void ErrorData(string error)
        {
            //Show the SuccessBox (to show the error in, of course) and hide the data
            SuccessBox.Visibility = Visibility.Visible;
            DataGrid.Visibility = Visibility.Collapsed;

            //Put the error in the successbox. Of course this line needed commented!
            SuccessBox.Text = error;
        }

        /// <summary>
        /// Change the data section of the screen to show the current data in the 0870db class
        /// </summary>
        private void ShowCurrentData()
        {
            //Show both the SuccessBox and the data grid
            SuccessBox.Visibility = Visibility.Visible;
            DataGrid.Visibility = Visibility.Visible;

            //Put the data in the grid
            SuccessBox.Text = "Success! Results:";
            CompanyDataBox.Text = db.Company;
            OrigNumberDataBox.Text = db.OrigNumber;
            AltNumberDataBox.Text = db.ReplacementNumber;
        }

        /// <summary>
        /// Launch Skype with the alternative number
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private async void SkypeLink_Click(object sender, RoutedEventArgs e)
        {
            //Ensure were not launching an empty link
            if (AltNumberDataBox.Text != string.Empty)
            {
                //Create the URI to launch from a string.
                Uri uri = new Uri(@"callto://+" + AltNumberDataBox.Text);

                //Launch the URI.
                bool success = await Windows.System.Launcher.LaunchUriAsync(uri);
            }
        }

        /// <summary>
        /// Do something on help button click
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void HelpButton_Click(object sender, RoutedEventArgs e)
        {
            //HelpText.Visibility = (HelpText.Visibility == Visibility.Collapsed ? Visibility.Visible : Visibility.Collapsed);

            if (HelpText.Opacity == 1.0f)
                HelpExit.Begin();
            else
                HelpEnter.Begin();
        }

        /// <summary>
        /// Add commands to the settings pane
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="args"></param>
        private void MainPage_CommandsRequested(SettingsPane sender, SettingsPaneCommandsRequestedEventArgs args)
        {
            //Clear out the current ApplicationCommands
            args.Request.ApplicationCommands.Clear();

            //Create the Privacy Policy command
            SettingsCommand privacyPref = new SettingsCommand("privacyPref", "Privacy Policy", (uiCommand) =>
            {
                LaunchPrivacyPolicy();
            });

            //Add the Privacy Policy command to the Settings pane
            args.Request.ApplicationCommands.Add(privacyPref);
        }

        /// <summary>
        /// Launch the URL to the privacy policy link
        /// </summary>
        private async void LaunchPrivacyPolicy()
        {
            //Create the link to the privacy policy and launch it
            Uri privacyLink = new Uri(privacyPolicyUrl);
            await Windows.System.Launcher.LaunchUriAsync(privacyLink);
        }
    }
}
